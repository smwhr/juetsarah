<?php
include(__DIR__.'/../scripts/bootstrap.php');

$photos = [];
$dir = opendir($app['root_dir']."/web/upload/full");
while (false !== ($entry = readdir($dir))) {
    if(substr($entry, -4) == ".jpg")
      $photos[] = substr($entry, 0, -4);
}