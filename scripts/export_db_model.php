<?php
include(__DIR__.'/../scripts/bootstrap.php');

$opt = $config['db.options'];

$cmd =  "mysqldump";
$cmd .= " -h" . $opt['host'];
$cmd .= " -u" . $opt['user'];
$cmd .= " -p" . $opt['password'];
$cmd .= " " . $opt['dbname'];
$cmd .= " --no-data --add-drop-table";

$cmd .= " > " . __DIR__.'/../config/model.sql';

exec($cmd);