<?php

$config['debug'] = true;

// DB connection info
$config['db.options']['dbname']   = 'juetsarah';
$config['db.options']['host']     = '127.0.0.1';
$config['db.options']['user']     = 'root';
$config['db.options']['password'] = '';

return $config;
