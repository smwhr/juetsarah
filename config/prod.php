<?php

$config['debug'] = false;

// DB connection info
$config['db.options']['dbname']   = 'projectdb_prod';
$config['db.options']['host']     = 'localhost';
$config['db.options']['user']     = 'DEV_USER';
$config['db.options']['password'] = 'DEV_PWD';
return $config;
