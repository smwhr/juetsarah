<?php

$config['debug'] = true;

// DB connection info
$config['db.options']['dbname']   = 'projectdb_local';
$config['db.options']['host']     = 'localhost';
$config['db.options']['user']     = 'DEV_USER';
$config['db.options']['password'] = 'DEV_PWD';

return $config;
