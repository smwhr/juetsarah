<?php

namespace Controller;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class FrontController
{
  public function indexAction(Application $app) {
    $response = new Response($app['twig']->render('index.html.twig'));
    return $response;
  }
  public function travelAction(Application $app) {
    $response = new Response($app['twig']->render('travel.html.twig'));
    return $response;
  }
  public function photosAction(Application $app, $default="IMG_0001") {

    $photos = [];
    $dir = opendir($app['root_dir']."/web/upload/full");
    while (false !== ($entry = readdir($dir))) {
        if(substr($entry, -4) == ".jpg")
          $photos[] = substr($entry, 0, -4);
    }
    sort($photos);

    $response = new Response($app['twig']->render('photos.html.twig',compact("photos","default")));
    return $response;
  }

  public function photoAction(Application $app, $id) {
    $dir = $app['root_dir']."/web/upload/full";
    if(!file_exists($dir."/".$id.".jpg")){
      $img = $app['root_dir']."/web/img/default.png";
    }else{
      $img = $dir."/".$id.".jpg";
    }

    header('Content-type: image/jpeg');
    $image = new \Imagick($img);
    $image->adaptiveResizeImage(900,700, true);

    $draw = new \ImagickDraw();
    $draw->setFont($app['root_dir']."/web/fonts/rupture-webfont.ttf");
    $draw->setFillColor("#0094d8");
    $draw->setFontSize(15);
    $draw->annotation(595, 590, "Julien et Sarah — Photo Lucie Sassiat");

    $image->setImageFormat("jpg");
    $image->drawImage($draw);

    echo $image;
    exit;
  }
  public function audioAction(Application $app) {
    $response = new Response($app['twig']->render('audio.html.twig'));
    return $response;
  }
  public function videoAction(Application $app) {
    $response = new Response($app['twig']->render('video.html.twig'));
    return $response;
  }
}

