<?php
use Silex\Application;
use Silex\Provider;
use Monolog\Handler\ChromePHPHandler;
use Monolog\Handler\RotatingFileHandler;
use User\UserProvider;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

/*
*  Init app and services providers
*/
$app            = new Silex\Application();
$app['debug']   = $config['debug'];
$app['config']  = $config;
$app['root_dir'] = realpath(__DIR__."/..");

$app->register(new Provider\UrlGeneratorServiceProvider());
$app->register(new Provider\SessionServiceProvider());
$app->register(new Provider\ServiceControllerServiceProvider());

// Twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
  // 'twig.options' => array('cache' => __DIR__.'/../cache', 'strict_variables' => true),
  'twig.options' => array('strict_variables' => true),
  'twig.path' => __DIR__.'/../views',
));

// Gestion des logs
$app->register(new Silex\Provider\MonologServiceProvider(), array(
  'monolog.logfile' => __DIR__.'/../log/all.log',
  'monolog.name' => 'boostrap'
));

// Cache
$app->register(new Provider\HttpCacheServiceProvider(), array(
  'http_cache.cache_dir' => __DIR__.'/../cache/'
));

$app->register(new Provider\DoctrineServiceProvider(), array('db.options' => $config['db.options']));
$app->register(new Alpha\ServiceProvider());

// Login
$app->register(new Silex\Provider\SecurityServiceProvider());
$app['security.encoder.digest'] = $app->share(function ($app) {
    return new MessageDigestPasswordEncoder('sha1', false, 1);
});
$app['security.firewalls'] = array(
  'video' => array(
    'pattern' => '^/video',
    'http' => true,
    'users' => array(
      //natamariage/natamariage
      'natamariage' => array('ROLE_ADMIN', '85db2efc49013a3ddc5c6d10b252282d6257d857'),
    ),
  ),
);

$app['security.role_hierarchy'] = array(
    'ROLE_ADMIN' => array('ROLE_EDITO'),
);