<?php
use Symfony\Component\HttpFoundation\Request;

/*
 *  FRONT
 */
$app->get('/', 'Controller\FrontController::indexAction')->bind('home');
$app->get('/travel', 'Controller\FrontController::travelAction')->bind('travel');
$app->get('/travel.html', 'Controller\FrontController::travelAction')->bind('travel2');
$app->get('/photos', 'Controller\FrontController::photosAction')->bind('photos');
$app->get('/photos/{default}', 'Controller\FrontController::photosAction')->bind('photos_default');
$app->get('/p/{id}', 'Controller\FrontController::photoAction')->bind('photo');
$app->get('/audio', 'Controller\FrontController::audioAction')->bind('audio');
$app->get('/video', 'Controller\FrontController::videoAction')->bind('video');

/*
 * AJAX
 */
$ajax = $app['controllers_factory'];
$ajax->match('/', 'Controller\AjaxController::indexAction')->bind('ajax');
$app->mount('/ajax', $ajax);
