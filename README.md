silex-bootstrap
===============

An empty PHP projet based on Silex, Bootstrap and AlphaManager

First of all : fork the current project


Install
=======
    git clone git@bitbucket.org:vodkaster/myproject.git
    curl -s http://getcomposer.org/installer | php
    php composer.phar install
    
Environment
===========

Apache
	
	<VirtualHost *:80>
    	ServerName www.mysite.com
    	DocumentRoot /path/to/mysite/web
    	SetEnv APPLICATION_ENV "local"
        <Directory "/path/to/mysite/web">
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>

Database : DB name, login & password to be set in config/local.php (or dev.php or prod.php)

Scripting : create the scripts/application_env.php file with the environment

	<?php
	return "local";
	
